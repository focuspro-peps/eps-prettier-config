# eps-prettier-config

In package.json, add this (replace TAG with the preferred versions):

```json
{
  "prettier": "@eps/prettier-config",
  "devDependencies": {
    "@eps/prettier-config": "git+https://bitbucket.org/focuspro-peps/eps-prettier-config.git#TAG",
    "prettier": "PRETTIER_VERSION"
  }
}
```